/*
1. Опишіть своїми словами, що таке метод об'єкту.
   Метод це функція, яка оголошується в середині об'єкту як його властивість (тобто є властивістю об'єкта), зазвичай взаємодіє з іншими властивостями об'єкту.

2. Який тип даних може мати значення властивості об'єкта?
  Властивість об'єкта може мати будь-який тип даних, що існує в JS, в т.ч. інший об'єкт.

3. Об'єкт це посилальний тип даних. Що означає це поняття?
  Це означає те, що при зверненні до об'єкта за його назвою передається посилання на місце у пам'яті, де зберігається об'єкт, а не копія об'єкта.
  Тому якщо створити нову змінну і через оператор присвоєння задати значення існуючого об'єкта, то передасться посилання на місце у пам'яті.
  І якщо у нового об'єкта змінити якусь властивість, вона буде змінена і у першого, бо фактично обидві змінні посилатимуться на одне місце у пам'яті.
  Тому для копіювання (клонування) об'єктів необхідно використовувати цикл for in (через key) або глобальний метод Object.assign().  
*/


  "use strict";

  // Написати функцію, яка буде створювати та повертати об'єкт.
function createNewUser() {
  let newUser = {
    firstName: "",
    lastName: "",
    // Додати метод, який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі
    getLogin() {
      return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
    },
    // Створити функції-сеттери, які дозволять змінити властивості
    setFirstName(newFirstName) {
      return Object.defineProperty (newUser, 'firstName', {
        value: newFirstName,
      });
    },
    setLastName(newLastName) {
      return Object.defineProperty (newUser, 'firstName', {
        value: newLastName,
      });
    },
  };
  // При виклику функція повинна запитати ім'я та прізвище.
  Object.defineProperty (newUser, 'firstName', {
      value: prompt ("Enter your name"),
      writable: false,
      configurable: true
    });
  Object.defineProperty (newUser,'lastName', {
      value: prompt ("Enter your surname"),
      writable: false,
      configurable: true
    });
  return newUser;
}

const myUser = createNewUser();

let myUserLogin = myUser.getLogin();

console.log(myUser);
console.log(myUserLogin);


